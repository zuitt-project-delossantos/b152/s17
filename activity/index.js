const students = ['Carlo', 'Adrian', 'Nikko', 'Alexis', 'Blue', 'Bianca']

function addStudent(name){
	// log the name of the student added and the new value of the students array
	console.log(name + ' was added to the student list')
	console.log(students)
}

function countStudents(){
	// count the number of students inside the students array
}

function printStudents(){
	// sorts the array in alphanumeric order
	// logs each student name in the browser (using forEach)
}

addStudent('Carlo')
addStudent('Adrian')
addStudent('Nikko')
addStudent('Alexis')
addStudent('Blue')
addStudent('Bianca')
console.log(countStudents('Nikko'))
console.log(printStudents('Alexis'))


function findStudent(name){

	if(name == students){
		console.log(name, ' is an enrollee.')
	}
	else{
		console.log(name, 'is not an enrollee.')
	}
}

students.forEach(function(name){

	if(students == 'Carlo'){
		console.log(students, 'is an enrollee.')
	}
	if(students == 'Adrian'){
		console.log(students, 'is an enrollee.')
	}if(students == 'Nikko'){
		console.log(students, 'is an enrollee.')
	}if(students == 'Alexis'){
		console.log(students, 'is an enrollee.')
	}if(students == 'Blue'){
		console.log(students, 'is an enrollee.')
	}if(students == 'Bianca'){
		console.log(students, 'is an enrollee.')
	}
})



findStudent('Keane')
