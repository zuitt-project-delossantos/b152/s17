// Storing multiple values using variables
let student1 = '2020-01-17'
let student2 = '2020-01-20'
let student3 = '2020-01-25'
let student4 = '2020-02-25'


console.log(student1)
console.log(student2)
console.log(student3)
console.log(student4)


// Storing multiple values in an array
const studentNumbers = ['2020-01-17', '2020-01-20', '2020-01-25', '2020-02-25'] //array of strings
console.log(studentNumbers)


// Arrays are declared using the square brackets AKA Array Literals
// Each data stored inside an array is called an array element
const emptyArray = [] //declares an empty array
const grades = [72, 85.5, 92, 94] //array of numbers
const computerBrands = ['Acer', 'Asus', 'Lenovo', 'Apple', 'Redfox', 'Gateway']

console.log('Empty Array Sample: ', emptyArray)
console.log(grades)
console.log('Computer Brands Array: ', computerBrands)


// Outputs the value of the element in the index 1 of the studentNumbers array
console.log(studentNumbers[1])





const fruits = ['Apple', 'Orange', 'Kiwi','Dragon Fruit']
console.log('Value of fruits array before push()', fruits)

// PUSH function adds an element at the end of an array
fruits.push('Mango')
console.log('Value of fruits array after push()', fruits)

// POP() removes the last element in an array
fruits.pop()
console.log('Value of fruits after pop()', fruits)

// UNSHIFT() adds one or more elements at the beginning of an array
fruits.unshift('strawberry')
console.log('Value of fruits after unshift', fruits)

// SHIFT() removes the first element in an array
fruits.shift()
console.log('Value of fruits after shift', fruits)

let removedFruit = fruits.shift() //expected output is Apple removed from the fruits array
console.log('You successfully removed the fruit:', removedFruit)
console.log(fruits)

// REVERSE, reerses the order of elements in an array
fruits.reverse()
console.log('Value of fruits after reverse', fruits)



const tasks = [
	'drink html',
	'eat JS',
	'inhale CSS',
	'bake sass'
]

// SORT moves the last element of an array to the beginning 
console.log('Value of tasks before sort()', tasks)
tasks.sort()
console.log('Value of tasks after sort()', tasks)


const oddNumbers = [1, 3, 9, 5, 7]

// arranges the elements in alphanumeric sequence
console.log('Value of oddNumbers before sort()', oddNumbers)
oddNumbers.sort()
console.log('Value of oddNumbers after sort()', oddNumbers)


const countries = ['US', 'PH', 'CAN', 'SG', 'PH']
//indexOf() finds the index of a given element where it is FIRST founf
let indexCountry = countries.indexOf('PH')
console.log('Index of PH: ', indexCountry)//1

//lastIndexOf() finds the index of a given element where it is LAST found
let lastIndexCountry = countries.lastIndexOf('PH')
console.log('Last instance of PH: ', lastIndexCountry)
console.log(countries)

// toString() converts an array into a single value that is separated by a comma
console.log(countries.toString())


const subTasksA = ['drink html', 'eat JS']
const subTasksB = ['inhale CSS', 'breathe sass']
const subTasksC = ['get git', 'be node']


// concat() joins 2 or more arrays
const subTasks = subTasksA.concat(subTasksB, subTasksC)
console.log(subTasks)

// join() converts an array into a single value, and is separated by a specified character
console.log(subTasks.join('@'))



// forEach() function iterates each array element.

const users = ['blue', 'alexis', 'bianca', 'nikko', 'adrian']
console.log(users[0])//blue
console.log(users[1])//alexis

// syntax: 
/*
arrayName.forEach(function(eachElement){
	console.log(eachElement)
})

*/


// forEach iterates each element inside an array
// we pass each element as the parameter of the function declared inside the forEach()

// we have users array
// we want to iterate the users array using the forEach function
// each element is stored inside the variable called user
// logged in the browser the value of each user / each element inside the users array
users.forEach(function(user){
	// console.log('Name of student: ', user)

	if(user == 'blue'){
		console.log('Name of student: ', user)
	}
})







// Mini Activity
// Goal: using the forEach(), log in the console all the even numbers inside the numberList

const numberList = [1,2,3,4,5,6,7,8,9]

numberList.forEach(function(number){

	if(number%2==0){
		console.log(number, 'is an even number')
	}
	else {
		console.log(number, 'is an odd number')
	}
})


// length property returns the number of elements inside an array
console.log(numberList.length)

// map() iterates each element and returns new array with different values depending on the result of the fuction's operation.
// map is useful when we will manipulate/change element inside our array
// map() allows us to not touch/manipulate the original array

const numbersData = numberList.map(function(number){
	return number * number
})

console.log(numbersData)
console.log(numberList)//not be manipulated



// Multidimensional arrays
const chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
]

console.log(chessBoard) //logs the whole chessBoard array
console.log(chessBoard[0])
console.log(chessBoard[0][1]) // specific element inside the array index 0




